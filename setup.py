import pathlib
from setuptools import setup

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()

# This call to setup() does all the work
setup(
    name="predymale",
    version="1.0.0",
    description="Use machine-learning approach to predict haplogroup from STRs data",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/delehef/predymale",
    author="Franklin Delehelle",
    author_email="franklin.delehelle@irit.fr",
    license="GPLv3",
    classifiers=[
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Programming Language :: Python :: 3",
    ],
    packages=["predymale"],
    include_package_data=True,
    install_requires=[
        "PyQt5",
        "scipy",
        "numpy",
        "sklearn",
        "scikit-learn",
    ],
    entry_points={
        "console_scripts": [
            "predymale=predymale.__main__:main",
        ]
    },
)
