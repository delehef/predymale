![PredYmale](icon.png)

# PredYMaLe

PredYMaLe is a tool dedicated to apply a machine learning approach to haplogroup prediction.

## Why should I use PredYMale?

You should use PredYMale if you have a set of STRs-repeat counts defined individuals with known haplogroups (a training
set), and would want to estimate the haplogroup of individuals defined by the same kind of data, but without known
haplogroup. Please note however that all the expected haplogroups should be present in the training set, as PredYMale
cannot create new haplogroups it has not been trained on.

![PredYMale example](sshot2.png)

# Installation

## Automatic

The easiest way to install predymale is to simply install it with the `pip` command from your python distribution.
It should be noted that PredYMale requires Python 3 to run. Thus, `pip --version` should refer to a version 3.x of
Python. Otherwise, it is a sign that you should either use the `pip3` command and/or install Python 3.

You can install PredYMale globally on your machine with `sudo pip install git+https://gitlab.com/delehef/predymale.git`
or for your user only with `pip install --user git+https://gitlab.com/delehef/predymale.git`.

## Manual

It should be noted that PredYMale has been developed for Python 3. In the following commands, it is assumed that `python` refers to a Python 3 interpreter. Depending on your setup, you might need to install Python 3 or to use
the `python3` interpreter.

1. First, you should [download PredYMale](https://gitlab.com/delehef/predymale/-/archive/master/predymale-master.zip).
Then extract the archive file and navigate to the newly created folder.

2. You should then [create a virtual environment](https://docs.python.org/3/library/venv.html) to locally install the dependencies of PredYMale. Under GNU/Linux or macOS, you can do so by using the following command: `python3 -m venv virtualenv`.

3. Activate the virtual environment you just created by sourcing the activation script: `source virtualenv/bin/activate`.

4. Install the needed libraries with `pip install -r requirements.txt`.

# Use

At this point, you can now launch PredYMale with `predymale` if you used the automatic install method, or `python predymale` in the `predymale` directory if you used the manual install method.

If PredYMale has been installed correctly, it will guide you throughout its use.

![PredYMale wizard](sshot.png)
