import sys
import csv
import time

import numpy as np

from collections import Counter


from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import QLabel, QVBoxLayout, QGridLayout, QTableWidget, QTableWidgetItem, QPushButton, QComboBox, QMessageBox, QApplication, QProgressDialog
from PyQt5.QtCore import pyqtSlot, QFileInfo, Qt


from sklearn.preprocessing import *
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier

def most_common(lst):
    return max(set(lst), key=lst.count)

def validate(x, pos):
    if x > 40:
        raise Exception("incorrect value for sample #{0}, STR #{1}: {2} is surprisingly high"
                        .format(pos[0] + 1, pos[1], x))

    if x == 0:
        raise Exception("incorrect value 0 for sample #{0}, STR #{1}"
                        .format(pos[0] + 1, pos[1]))

def conflicts(names):
    c = Counter(names)
    if len(c.keys()) == 1:
        return ""
    else:
        r = ""
        for item in c.items():
            r += f"{item[0]}[+{item[1]}] "
        return r

class IntroPage(QtWidgets.QWizardPage):
    def  __init__(self, parent=None):
        super(IntroPage, self).__init__(parent)
        self.setTitle("Welcome to PredYMaLe!")
        label = QLabel("""
This application will guide you through the process of training a machine learning system on a labelled dataset, then using the systen to predict haplogroups on unknown data.<br>

<h3>Training</h3>
For the training step, you will need a CSV file containing all your samples. For each sample, it should have <b>a column containing its haplogroup</b>, while <b>the other ones should contain the number of corresponding STRs</b>.<br>
You might have a comment line (starting with a '#') containing the headers of the columns. The machine learning system will then be trained on this dataset.

<h3>Labelling</h3>
Once the machine learning system is ready, you might use it to determine the haplogroup of an unlabelled set of samples from their STRs repeat counts.<br>
You will have to input them as a CSV file containing <b>a column with the IDs of your samples</b>, then <b>a set of columns with the STRs repeat counts</b>.<br>
The STRs included in this dataset must be the same, <i>and in the same order</i>, than in the training dataset. Otherwise, the results will be garbage.<br>

<h3>Results</h3>
PredYMaLe will then present you with the predicted haplogroups for your dataset. You can save these predictions in a CSV file for further processing.

<i>It should be noted that CSV files can be easily converted from XLS(X) files by Microsoft Excel export tool.</i>
""")
        label.setWordWrap(True)
        l = QVBoxLayout()
        l.addWidget(label)
        self.setLayout(l)


class TrainingSetPage(QtWidgets.QWizardPage):
    def  __init__(self, parent=None):
        super(TrainingSetPage, self).__init__(parent)
        self.table = QTableWidget()
        self.ok = False
        self.labelBox = QComboBox()
        self.openFileButton = QPushButton("Open file...")
        self.openFileButton.clicked.connect(self.openFile)
        self.label = QLabel()

        self.setTitle("1/3 – Chose a training dataset")
        self.setSubTitle("Please chose a CSV file containing your labelled dataset.<br>It should contain <b>a column with the haplogroups</b> and other columns representing the <b>STRs repetition count</b>.")
        l = QGridLayout()
        l.addWidget(self.openFileButton, 0, 0, 1, 2)
        l.addWidget(self.label, 1, 0, 1, 2)
        l.addWidget(self.table, 2, 0, 1, 2)
        l.addWidget(QLabel("Haplogroups column: "), 3, 0)
        l.addWidget(self.labelBox, 3, 1)
        self.setLayout(l)

    def isComplete(self):
        return self.ok

    def clear(self):
        self.ok = False
        self.labelBox.clear()
        self.label.setText("")
        self.table.clear()
        self.table.setRowCount(0)
        self.table.setColumnCount(0)
        self.headers = []
        self.data = []


    @pyqtSlot()
    def openFile(self):
        filename = QtWidgets.QFileDialog.getOpenFileName(self, "Open Training dataset", "", "Delimiter separated filed (*.csv *.tsv)")[0]
        if not filename: return

        self.clear()
        try:
            with open(filename, newline='') as csvfile:
                dialect = csv.Sniffer().sniff(csvfile.read(4096)); csvfile.seek(0)
                has_header = csv.Sniffer().has_header(csvfile.read(4096)); csvfile.seek(0)
                reader = csv.reader(csvfile, dialect)

                if has_header: self.headers = reader.__next__()
                for row in reader: self.data.append(row)
                if not self.headers: self.headers = [str(i) for i in range(len(self.data[0]))]

            self.labelBox.addItems(self.headers)
            self.table.setColumnCount(len(self.headers))
            self.table.setHorizontalHeaderLabels(self.headers)
            for sample_id, sample in enumerate(self.data):
                self.table.insertRow(self.table.rowCount())
                for i, _x in enumerate(sample):
                    x = _x.strip()
                    if not x:
                        raise Exception("No value at <%d, %d>"%(sample_id+1, i))
                    self.table.setItem(self.table.rowCount() - 1, i, QTableWidgetItem(x))

            self.ok = True
            self.label.setText("<b>File opened: </b> %s"%filename)
            self.completeChanged.emit()
        except Exception as e:
            QMessageBox.critical(self, "CSV reading error", f"Unable to open {filename}: {str(e)}")
            self.clear()

    def validatePage(self):
        try:
            label_i = self.labelBox.currentIndex()
            labels = np.array([x[label_i].strip() for x in self.data])

            data = np.array(self.data)
            data = np.delete(data, label_i, 1)
            data = np.array(data, dtype=np.float)
            it = np.nditer(data, flags=['multi_index'])
            while not it.finished:
                validate(it[0], it.multi_index)
                it.iternext()

            self.headers.pop(label_i)
            self.wizard().set_traininig_data(labels, data, self.headers)
            return True
        except Exception as e:
            QMessageBox.critical(self, "Data error", f"<b>Error</b> {str(e)}<br>Have you selected the correct haplogroup column?")
            return False


class DataSetPage(QtWidgets.QWizardPage):
    def  __init__(self, parent=None):
        super(DataSetPage, self).__init__(parent)
        self.table = QTableWidget()
        self.idBox = QComboBox()
        self.ok = False
        self.label = QLabel()
        self.openFileButton = QPushButton("Open file...")
        self.openFileButton.clicked.connect(self.openFile)

        self.setTitle("2/3 – Enter your unknown dataset")
        self.setSubTitle("In this step, you will chose the dataset for wich you wish to determine haplogroups. Please open a CSV file with one sample per line, one column with their IDs, and as many remaining columns as there were STRs used in you training dataset.")

        l = QGridLayout()
        l.addWidget(self.openFileButton, 0, 0, 1, 2)
        l.addWidget(self.label, 1, 0, 1, 2)
        l.addWidget(self.table, 2, 0, 1, 2)
        l.addWidget(QLabel("IDs column: "), 3, 0)
        l.addWidget(self.idBox, 3, 1)
        self.setLayout(l)

    def isComplete(self):
        return self.ok

    def clear(self):
        self.ok = False
        self.table.clear()
        self.table.setRowCount(0)
        self.table.setColumnCount(0)
        self.headers = []
        self.data = []

    @pyqtSlot()
    def openFile(self):
        filename = QtWidgets.QFileDialog.getOpenFileName(self, "Open dataset to label", "", "Delimiter separated filed (*.csv *.tsv)")[0]
        if not filename: return

        self.clear()
        try:
            with open(filename, newline='') as csvfile:
                dialect = csv.Sniffer().sniff(csvfile.read(4096)); csvfile.seek(0)
                has_header = csv.Sniffer().has_header(csvfile.read(4096)); csvfile.seek(0)
                reader = csv.reader(csvfile, dialect)

                if has_header: self.headers = reader.__next__()
                for row in reader: self.data.append(row)
                if not self.headers: self.headers = [str(i) for i in range(len(self.data[0]))]

            self.idBox.addItems(self.headers)
            self.table.setColumnCount(len(self.headers))
            self.table.setHorizontalHeaderLabels(self.headers)
            for sample in self.data:
                self.table.insertRow(self.table.rowCount())
                for i, x in enumerate(sample):
                    self.table.setItem(self.table.rowCount() - 1, i, QTableWidgetItem(x))

            self.ok = True
            self.label.setText("<b>File opened: </b> %s"%filename)
            self.completeChanged.emit()
        except Exception as e:
            QMessageBox.critical(self, "CSV reading error", f"Unable to open {filename}: {str(e)}")


    def validatePage(self):
        try:
            id_i = self.idBox.currentIndex()
            ids = np.array([x[id_i] for x in self.data])

            data = np.array(self.data)
            data = np.delete(data, id_i, 1)
            data = np.array(data, dtype=np.float)

            self.wizard().set_to_label(data, ids)
            return True
        except Exception as e:
            QMessageBox.critical(self, "Data error", f"<b>Error</b> {str(e)}<br>Have you selected the correct IDs column?")
            return False

class ResultPage(QtWidgets.QWizardPage):
    def  __init__(self, parent=None):
        super(ResultPage, self).__init__(parent)
        self.setTitle("3/3 – Predicted haplogroups")
        self.table = QTableWidget()
        self.exportCSVButton = QPushButton("Export as CSV...")
        self.exportCSVButton.clicked.connect(self.exportCSV)

        l = QVBoxLayout()
        l.addWidget(self.exportCSVButton)
        l.addWidget(self.table)
        self.setLayout(l)

    def initializePage(self):
        self.table.setColumnCount(len(self.wizard().predictions.keys()) + 1)
        self.table.setRowCount(len(self.wizard().predictions[next(iter(self.wizard().predictions))]))
        self.table.setHorizontalHeaderLabels(["Sample ID"] + list(self.wizard().predictions.keys()))
        self.table.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        for i in range(len(self.wizard().ids)):
            self.table.setItem(i, 0, QTableWidgetItem(self.wizard().ids[i]))
        for i, name in enumerate(self.wizard().predictions.keys()):
            for j, p in enumerate(self.wizard().predictions[name]):
                self.table.setItem(j, i+1, QTableWidgetItem(p))


    @pyqtSlot()
    def exportCSV(self):
        filename = QtWidgets.QFileDialog.getSaveFileName(self, "Export results", "", "Separator delimited files (*.csv)")[0]
        samples_count = len(self.wizard().predictions[next(iter(self.wizard().classifiers))])
        with open(filename, 'w') as f:
            f.write("# Sample ID;%s;Conflicts;Consensus\n"%";".join([name + " prediction" for name in self.wizard().classifiers.keys()]))
            for sample in range(samples_count):
                f.write("%s;%s\n"%(self.wizard().ids[sample],
                                   ";".join([self.wizard().predictions[name][sample] for name in self.wizard().predictions.keys()]))
                )


class PredYMaLe(QtWidgets.QWizard):
    def __init__(self, parent=None):
        super(PredYMaLe, self).__init__(parent)
        self.setWindowIcon(QtGui.QIcon('icon.png'))
        self.setWindowTitle('PredYMaLe')

        self.setOption(QtWidgets.QWizard.NoBackButtonOnStartPage)
        self.setOption(QtWidgets.QWizard.NoCancelButton)

        self.addPage(IntroPage(self))
        self.addPage(TrainingSetPage(self))
        self.addPage(DataSetPage(self))
        self.addPage(ResultPage(self))

        self.training_data = []
        self.data = []


    def train(self):
        QApplication.setOverrideCursor(Qt.WaitCursor)
        self.classifiers = {
            "k-NN"         : KNeighborsClassifier(),
            "Linear SVM"   : SVC(kernel="linear"),
            "Random Forest": AdaBoostClassifier(base_estimator=RandomForestClassifier(n_estimators=10, max_features="auto")),
            "Neural Net"   : MLPClassifier((50, 10), max_iter=300),
        }
        for name in self.classifiers:
            self.classifiers[name].fit(self.training, self.labels)
        QApplication.restoreOverrideCursor()


    def predict(self):
        self.predictions = {}
        for name in self.classifiers: self.predictions[name] = []
        self.predictions["Conflicts"] = []
        self.predictions["Consensus"] = []

        for i in range(len(self.to_label)):
            x = self.to_label[i]
            for name in self.classifiers:
                self.predictions[name].append(self.classifiers[name].predict(x.reshape(1, -1))[0])
            self.predictions["Conflicts"].append(conflicts([self.predictions[name][i] for name in self.classifiers.keys()]))
            self.predictions["Consensus"].append(most_common([self.predictions[name][i] for name in self.classifiers.keys()]))


    def set_traininig_data(self, labels, data, data_headers):
        self.labels = labels
        self.training = data
        self.data_headers = data_headers
        self.train()


    def set_to_label(self, data, ids):
        self.ids = ids
        self.to_label = data
        if self.to_label.shape[1] != len(self.data_headers):
            raise Exception("the training dataset has %d STRs; the dataset to label has %d"%(len(self.data_headers), self.to_label.shape[1]))
        self.predict()


def main():
    app = QtWidgets.QApplication(sys.argv)
    process = PredYMaLe()
    process.show()
    sys.exit(process.exec_())


if __name__ == '__main__':
    main()
